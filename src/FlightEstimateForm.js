import React, { useState } from 'react';
import './FlightEstimateForm.css';

function FlightEstimateForm() {
  const [formData, setFormData] = useState({
    type: 'flight',
    passengers: 1,
    legs: [
      { departure_airport: '', destination_airport: '' },
      { departure_airport: '', destination_airport: '' },
    ],
    distance_unit: 'km', 
    cabin_class: 'economy', 
  });

  const [result, setResult] = useState(null);

  const handleInputChange = (e, index) => {
    const { name, value } = e.target;

    // Update both legs' departure and destination airports
    const updatedLegs = [...formData.legs];

    if (name === "departure_airport") {
      updatedLegs[0].departure_airport = value;
      updatedLegs[1].destination_airport = value;
    } else if (name === "destination_airport") {
      updatedLegs[0].destination_airport = value;
      updatedLegs[1].departure_airport = value;
    }

    setFormData({ ...formData, legs: updatedLegs });
  };

  const handlePassengerChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch('http://localhost/flight-estimate', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });

      if (response.ok) {
        const data = await response.json();
        setResult(data);
      } else {
        console.error('Request failed');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <div>
      <h2>Flight Emissions Estimate</h2>
      <form onSubmit={handleSubmit}>
        <div>
          <label>
            Destination Airport:
            <input
              type="text"
              name="departure_airport"
              value={formData.legs[0].departure_airport}
              placeholder="Departure airport"
              onChange={(e) => handleInputChange(e, 0)}
            />
          </label>
        </div>
        <div>
          <label>
            Departure Airport:
            <input
              type="text"
              name="destination_airport"
              value={formData.legs[0].destination_airport}
              placeholder="Arrival airport"
              onChange={(e) => handleInputChange(e, 0)}
            />
          </label>
        </div>
        <div>
          <label>
            Number of Passengers:
            <input
              type="number"
              name="passengers"
              value={formData.passengers}
              onChange={handlePassengerChange}
            />
          </label>
        </div>
        <div>
          <label>
            Distance Unit:
            <select
              name="distance_unit"
              value={formData.distance_unit}
              onChange={handlePassengerChange}
            >
              <option value="km">Kilometers</option>
              <option value="mi">Miles</option>
            </select>
          </label>
        </div>
        <div>
          <label>
            Cabin Class:
            <select
              name="cabin_class"
              value={formData.cabin_class}
              onChange={handlePassengerChange}
            >
              <option value="economy">Economy</option>
              <option value="premium">Premium</option>
            </select>
          </label>
        </div>
        <button type="submit">Calculate Emissions</button>
      </form>
      <br></br>
      {result && (
        <div>
          <h2>Results</h2>
          <p>Carbon Emissions: {result.data.attributes.carbon_g} g</p>
          <p>Distance: {result.data.attributes.distance_value} {result.data.attributes.distance_unit}</p>
        </div>
      )}
    </div>
  );
}

export default FlightEstimateForm;
