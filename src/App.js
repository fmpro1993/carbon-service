// src/App.js
import React from 'react';
import './App.css';
import FlightEstimateForm from './FlightEstimateForm';

function App() {
  return (
    <div className="App">
      <FlightEstimateForm />
    </div>
  );
}

export default App;
