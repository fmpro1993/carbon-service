
# Flight Emissions Estimate Application
This application provides a simple interface for estimating carbon emissions for a flight based on the departure and arrival airports, the number of passengers, the distance unit (kilometers or miles), and the cabin class (economy or premium). It consists of both a client-side React application and a Node.js backend server.

## Client Side (React)
FlightEstimateForm Component
The client-side of the application is built using React. The main component responsible for the user interface is FlightEstimateForm. It includes the following features:

## Input fields for entering the departure and arrival airports.
A field for specifying the number of passengers.
Dropdown menus for selecting the distance unit (kilometers or miles) and cabin class (economy or premium).
A button to calculate the emissions estimate.
Displaying the calculated emissions results.
Usage
Enter the departure and arrival airports (both ways).
Specify the number of passengers.
Select the distance unit and cabin class.
Click the "Calculate Emissions" button to submit the form.
View the emissions results once they are calculated.
Backend (Node.js)
The backend server for this application is a Node.js application using Express. It handles the API requests and communicates with the Carbon Interface API to estimate carbon emissions for the provided flight details.

## Carbon CLI (Command Line Interface)
The backend also includes a CLI tool (carbon-cli) that allows users to calculate carbon emissions from the command line. This is useful for making emissions calculations without using the web interface.

## Start via docker images
```
docker pull fmpro/carbone-cli:latest
docker run -d -p 80:80 -e API_KEY=<your api key> --name carbon fmpro/carbone-cli
```
Now you can access GUI interface of the tool via http://localhost

To access CLI interface use command below:

```
docker exec carbon node server/app.js calculate -d <departure airport> -a <arrival airport> -p <number of passengers> -u <units optional> -c <class optional>
```

For example, to calculate carbon emissions in miles from TLV airport to LHR and return with 4 passengers flying premium class, you can use the command below:
```
docker exec carbon node server/app.js calculate -d TLV -a LHR -p 4 -u mi -c premium
```
## Start without docker
To start the server run
```
npm install
cd server && npm install && cd ..
npm run build
export API_KEY=<your api key>
npm start 
```
Then you can start using cli tool and as well Frontend application, to use fronted application simply access http://localhost/

To use CLI tool use the following instructure

Open new terminal and run
```
node server/app.js help calculate
Usage: carbon-cli calculate [options]

Calculate carbon emission

Options:
  -d, --departure <name>     airport of departure
  -a, --arrival <name>       airport of arrival
  -p, --passengers <number>  Number of passengers
  -u, --unit <name>          unit to calculate km or ml (default: "km")
  -c, --class <name>         The seating class that the passengers are taking for the leg, economy or premium (default: "economy")
  -h, --help                 display help for command

node carbon-cli calculate -d <departure> -a <arrival> -p <passengers> -u <unit> -c <class>
```
Replace <departure>, <arrival>, <passengers>, <unit>, and <class> with the desired values.

For example, to calculate carbon emissions in miles from TLV airport to LHR and return with 4 passengers flying premium class, you can use the command below:
```
node server/app.js calculate -d TLV -a LHR -p 4 -u mi -c premium
```
The CLI will communicate with the server and display the emissions results in the command line.

## Server Configuration
The server runs on port 80 and serves the React client build. It also handles API requests to estimate carbon emissions. The server expects an API key from the Carbon Interface service, which should be stored in a .env file or provided via export API_KEY=<your api >.

## Technologies Used
React for the client-side user interface.
Node.js and Express for the server.
Axios for making HTTP requests.

## Configure the server and API key as mentioned above.
Set up the client-side build.
Start the server.
Dependencies
React
Node.js
Express
Axios
Carbon Interface API
Please make sure to install the necessary dependencies using npm.

Feel free to reach out for any questions or further assistance regarding the Flight Emissions Estimate Application.