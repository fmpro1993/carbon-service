FROM node:14

WORKDIR /app

COPY package*.json ./

COPY build ./build

COPY server ./server

RUN mkdir -p /app/data

RUN cd server && npm install

EXPOSE 80

CMD ["npm", "start"]
