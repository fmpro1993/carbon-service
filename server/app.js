const { Command } = require('commander');
const program = new Command();
const axios = require('axios');

program
  .name('carbon-cli')
  .description('CLI to calculate carbon emission.')
  .version('0.1.0');

  program.command('calculate')
  .description('Calculate carbon emission')
  .option('-d, --departure <name>', 'airport of departure')
  .option('-a, --arrival <name>', 'airport of arrival')
  .option('-p, --passengers <number>', 'Number of passengers') 
  .option('-u, --unit <name>', 'unit to calculate km or ml', 'km')
  .option('-c, --class <name>', 'The seating class that the passengers are taking for the leg, economy or premium', 'economy')
  .action(async (options) => {
    const formData = {
      type: 'flight',
      passengers: options.passengers,
      legs: [
        { departure_airport: options.departure, destination_airport: options.arrival },
        { departure_airport: options.arrival, destination_airport: options.departure },
      ],
      distance_unit: options.unit,
      cabin_class: options.class,
    };

    try {
      const response = await axios.post('http://localhost/flight-estimate', formData, {
        headers: {
          'Content-Type': 'application/json'
        },
      });

      if (response.status === 200) {
         console.log('Emission calculation successful.');
         console.log('Results:', response.data);
      } else {
        console.error('Request failed');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  });
  program.parse();
