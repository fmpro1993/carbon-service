const express = require('express');
const path = require('path');
const app = express();
const port = 80;
const cors = require('cors');
const axios = require('axios');
app.use(cors());
require('dotenv').config()
app.use(express.static(path.join(__dirname, '../build')));
app.use(express.json());
const api_key = process.env.API_KEY
app.post('/flight-estimate', async (req, res) => {
  const { type, passengers, legs, distance_unit, cabin_class } = req.body;
  if (type !== 'flight' || !passengers || !legs || legs.length < 2) {
    return res.status(400).json({ error: 'Invalid request data' });
  }
  if (api_key === undefined) {
    return res.status(400).json({ error: 'Api token undefined' });
  }

  const requestPayload = {
    type,
    passengers,
    legs,
    distance_unit: distance_unit || 'km',
    cabin_class: cabin_class || 'economy'
  };

  try {
    const response = await axios.post('https://www.carboninterface.com/api/v1/estimates', requestPayload, {
      headers: {
        'Authorization': `Bearer ${api_key}`,
        'Content-Type': 'application/json',
      },
    });

    const carbonEmissions = response.data;
    res.json(carbonEmissions);
  } catch (error) {
    console.error('API request error:', error);
    res.status(500).json({ error: 'An error occurred while fetching emissions data' });
  }
});

if (!module.parent) {
  app.listen(port);
}

module.exports = app;
